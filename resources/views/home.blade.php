<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Utiliza nuestra herramienta para codificar URL y también para decodificar URL. Codifica y decodifica URLs sin complicaciones.">
        <title>URLED</title>

<link href="/css/custom.css" rel="stylesheet"/>
        <link href="/css/base.css" rel="stylesheet"/>
<script src="/js/scripts_footer.min.js"></script>
        <script src="/js/scripts_header.min.js"></script>



    </head>
    <body>
        <div class="row">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="small-12  small-centered medium-8 medium-centered large-8 columns large-centered divTexto">
            <h1>Codifica y Decodifica URLs Online</h1>
            <h2>Herramienta para codificar y decodificar URLs</h2>
            <textarea  id="textoED" rows="6" cols="40" placeholder="Escriba su URL"></textarea>
            <div style="display: none;" id="contTotRes">
              <span>Resultado</span>
              <div class="small-12 panel" id="divCont" >

              </div>
            </div>
            <div class="row">
              <div class="small-12  medium-6 large-6 columns divBtn">
                <button type="button" id="encode">Encode</button>
                </div>
                <div class="small-12  medium-6 large-6 columns divBtn">
                <button type="button" id="decode">Decode</button>
              </div>
            </div>
            <div class="row">
              <div class="divExplicacion small-12 columns">
                <span>Herramienta de codificación y decodificación de URL</span>
                <div>
                  Utilice la herramienta para codificar o decodificar una cadena de texto. Para la interoperabilidad mundial, los URIs tienen que codificarse uniformemente. Para mapear la amplia gama de caracteres utilizados en todo el mundo en los 60 caracteres permitidos en un URI, se utiliza un proceso de dos pasos:<br><br>

                  <ul>
                    <li>Convierta la cadena de caracteres en una secuencia de bytes utilizando la codificación UTF-8.</li>
                    <li>Convierta cada byte que no sea una letra o dígito ASCII a% HH, donde HH es el valor hexadecimal del byte</li>
                  </ul>

                  Por ejemplo, la cadena: Alcobaça, sería codificada como: Alcoba%C3%A7a <br><br>

                  (El "ç" está codificado en UTF-8 como dos bytes C3 (hex) y A7 (hex), que luego se escriben como los tres caracteres "%c3" y "%a7", respectivamente). (Hasta 9 caracteres ASCII para un solo carácter Unicode), pero la intención es que los navegadores sólo necesiten mostrar el formulario decodificado, y muchos protocolos pueden enviar UTF-8 sin el escape% HH.
                </div>

                <span>¿Qué es la codificación URL?</span>
                <div>
                  La codificación de URL consta en codificar ciertos caracteres de una URL, reemplazándolos por uno o más caracteres que consisten en el carácter de porcentaje "%" seguido por dos dígitos hexadecimales. Los dos dígitos hexadecimales representan el valor numérico del carácter reemplazado.<br><br>
                  El término codificación de URL es un poco inexacto porque el procedimiento de codificación no se limita a URL (Uniform Resource Locator), sino que también puede aplicarse a cualquier otro URI (Uniform Resource Identifier) como URN (Uniform Resource Name).<br><br>
                </div>

                <span>¿Qué caracteres se permiten en una URL?</span>
                <div>
                  Los caracteres permitidos en un URI son reservados o no reservados. Los caracteres reservados son aquellos caracteres que a veces tienen un significado especial, mientras que los otros caracteres no tienen ese significado. Usando la codificación "%", los caracteres que de otro modo no se permitirían se representan usando caracteres permitidos.<br><br>
                  Los caracteres no reserva se pueden codificar, pero no deben codificarse. Los caracteres no reservas son:<br><br>
                  <div class="caracterees">
                    A B C D E F G H I J K L M N O P Q R S T U V W X Y Z <br>
                    a b c d e f g h i j k l m n o p q r s t u v w x y z <br>
                    0 1 2 3 4 5 6 7 8 9 - _ . ~
                  </div>
                  <br>
                  Los caracteres reservados tienen que ser codificados sólo bajo ciertas circunstancias, estos son: <br> <br>
                  <div class="caracterees">
                    ! * ' ( ) ; : @ & = + $ , / ? % # [ ]
                  </div>
                </div>

                <span>Cuándo y por qué utilizaría la codificación de URL?</span>
                <div>
                  Cuando se envían datos que se han introducido en formularios HTML, los nombres y valores de los campos de formulario se codifican y se envían al servidor en un mensaje de solicitud HTTP.<br><br>
                  Cuando se envía en una solicitud HTTP GET los datos de se incluyen en el componente de consulta del URI, a diferencia de cuando se envía en una solicitud HTTP POST o por correo electrónico en donde los datos se colocan en el cuerpo del mensaje y el tipo del medio se incluye en el encabezado Content-Type del mensaje.
                </div>
              </div>
            </div>
          </div>
        </div>

    </body>
</html>
