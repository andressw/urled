
$( document ).ready(function() {

  $.ajaxSetup(
{
  headers:
  {
      'X-CSRF-Token': $('input[name="_token"]').val()
  }
});


      $("#encode").click(function(){
      var urlED = $("#textoED").val();
      if(urlED.length > 0){
          $.post("/encode", {url:urlED})

          .done(function(data) {
            if($("#contTotRes").css('display')==="none"){
              //console.log("entro hidden");
              $("#divCont").html('<pre class="text-left" id="resED">'+data+'</pre>');
              $("#contTotRes").slideDown("800");
            }else{
              //console.log("entro no hidden");
              $("#resED").fadeOut("800", function(){
                $(this).html(data).fadeIn("800");
              });
            }

          }).fail(function() {
            if($("#contTotRes").css('display')==="none"){
              //console.log("entro hidden");
              $("#divCont").html('<pre class="text-left" id="resED">Se produjo un error inesperado, intentelo nuevamente.</pre>');
              $("#contTotRes").slideDown("800");
            }else{
              //console.log("entro no hidden");
              $("#resED").fadeOut("800", function(){
                $(this).html("Se produjo un error inesperado, intentelo nuevamente.").fadeIn("800");
              });
            }
          });
      }else{
          if($("#contTotRes").css('display')==="none"){
            //console.log("entro hidden");
            $("#divCont").html('<pre class="text-left" id="resED">Ingrese un texto.</pre>');
            $("#contTotRes").slideDown("800");
          }else{
            //console.log("entro no hidden");
            $("#resED").fadeOut("800", function(){
              $(this).html("Ingrese un texto.").fadeIn("800");
            });
          }
      }
    });

    $("#decode").click(function(){
    var urlED = $("#textoED").val();
    if(urlED.length > 0){
        $.post("/decode", {url:urlED})

        .done(function(data) {
          if($("#contTotRes").css('display')==="none"){
            //console.log("entro hidden");
            $("#divCont").html('<pre class="text-left" id="resED">'+data+'</pre>');
            $("#contTotRes").slideDown("800");
          }else{
            //console.log("entro no hidden");
            $("#resED").fadeOut("800", function(){
              $(this).html(data).fadeIn("800");
            });
          }

        }).fail(function() {
          if($("#contTotRes").css('display')==="none"){
            //console.log("entro hidden");
            $("#divCont").html('<pre class="text-left" id="resED">Se produjo un error inesperado, intentelo nuevamente.</pre>');
            $("#contTotRes").slideDown("800");
          }else{
            //console.log("entro no hidden");
            $("#resED").fadeOut("800", function(){
              $(this).html("Se produjo un error inesperado, intentelo nuevamente.").fadeIn("800");
            });
          }
        });
    }else{
        if($("#contTotRes").css('display')==="none"){
          //console.log("entro hidden");
          $("#divCont").html('<pre class="text-left" id="resED">Ingrese un texto.</pre>');
          $("#contTotRes").slideDown("800");
        }else{
          //console.log("entro no hidden");
          $("#resED").fadeOut("800", function(){
            $(this).html("Ingrese un texto.").fadeIn("800");
          });
        }
    }
  });
});
